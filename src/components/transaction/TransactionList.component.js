import React, { Component } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { formatRelative } from "date-fns";

import { selectScreenWidth } from "../../redux/screen/screen.selectors";
import { selectLoadingListTransaction, selectTransactionsGroupByOrder } from "../../redux/order/order.selectors";
import { listTransactionStart } from "../../redux/order/order.actions";

import { withRouter } from "react-router";

import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import CommonLoading from "../common/CommonLoading.component";
import NumberFormat from "react-number-format";
import { IS_MOBILE, TRANSACTION_STATUS, ORDER_STATUS, NODE_ENV } from "../../config";

class TransactionList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expandedRows: []
    }
  }
  componentDidMount() {
    this.props.listTransactionStart(100, 0);
  }
  componentDidUpdate(prevProps) {
    const { screenWidth } = this.props;
    if (screenWidth !== IS_MOBILE) {
      if (prevProps.loadingListTransaction !== this.props.loadingListTransaction) {
        if (this.props.loadingListTransaction === false) {
          const defaultExpandedRows = this.props.transactionsGroupByOrder.map((element) => {
            if (element.status !== 'completed')
              return { index: true };
            else {
              return { index: false };
            }
          })
          this.setState({ expandedRows: defaultExpandedRows });
        }
      }
    }

  }
  renderSort() {
    const { screenWidth } = this.props;
    if (screenWidth !== IS_MOBILE)
      return (
        <span className="uk-text-meta uk-text-muted uk-margin-small-left">
          <i className="fa fa-long-arrow-up"></i>
          <i className="fa fa-long-arrow-down"></i>
        </span>
      )
  }
  render() {

    const { loadingListTransaction, transactionsGroupByOrder, screenWidth } = this.props;

    const columnsGroupByOrder = [
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Order No.</span>
              {this.renderSort()}
            </div>
          )
        },
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        id: 'numericalOrder',
        accessor: 'row',
        className: 'uk-flex uk-flex-middle uk-flex-center',
        Cell: props => <span>Order {props.index + 1}</span>
      },
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Status</span>
              {this.renderSort()}
            </div>
          )
        },
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        accessor: 'status',
        className: 'uk-flex uk-flex-middle uk-flex-center',
        Cell: props => <span>{ORDER_STATUS[props.value]}</span>
      },
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Total</span>
              {this.renderSort()}
            </div>
          )
        },
        accessor: 'totalMoney',
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        className: 'uk-flex uk-flex-middle uk-flex-center',
        Cell: props => {
          if (props.value > 0)
            return <span><NumberFormat value={Math.abs(props.value)} displayType="text" thousandSeparator={true} prefix={'$'} /></span>
          else if (props.value == 0)
            return <span>No pay</span>
          else {
            return (
              <div>
                <b>You get: </b>
                <NumberFormat value={Math.abs(props.value)} displayType="text" thousandSeparator={true} prefix={'$'} />
              </div>
            )
          }
        }
      },
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Created at</span>
              {this.renderSort()}
            </div>
          )
        },
        accessor: 'createdAt',
        minWidth: screenWidth === IS_MOBILE ? 200 : 100,
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        className: 'uk-flex uk-flex-middle',
        Cell: props => <span>{formatRelative(new Date(props.value), new Date())}</span>
      }
    ];

    const columns = [
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Transaction Code</span>
              {this.renderSort()}
            </div>
          )
        },
        accessor: 'transactionCode',
        minWidth: screenWidth === IS_MOBILE ? 150 : 100,
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        className: 'uk-flex uk-flex-middle uk-flex-center uk-text-bold uk-text-secondary',
        Cell: props => <span>{props.value}</span>
      },
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Device</span>
              {this.renderSort()}
            </div>
          )
        },
        id: 'proposalExchange',
        minWidth: screenWidth === IS_MOBILE ? 200 : 100,
        accessor: d => {
          const { proposalExchange, proposalSale } = d;
          if (proposalExchange) {
            const { device, proposalExchangesDevices } = proposalExchange;
            if (device) {
              const deviceSeller = device.modelName;
              if (proposalExchangesDevices) {
                const proposalExchangeDevice = proposalExchangesDevices[0];
                const deviceBuyer = proposalExchangeDevice.modelName;
                return (
                  <div>
                    <p className="uk-margin-remove">{deviceSeller}</p>
                    <span className="uk-text-meta uk-text-muted"><b>Exchange with </b>{deviceBuyer}</span>
                  </div>
                )
              }
            }
          }
          if (proposalSale) {
            const { device } = proposalSale;
            if (device) {
              return (
                <div>
                  <p className="uk-margin-remove">{device.modelName}</p>
                </div>
              )
            }
          }
        },
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        className: 'uk-flex uk-flex-middle',
      },
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Status</span>
              {this.renderSort()}
            </div>
          )
        },
        accessor: 'status',
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        className: 'uk-flex uk-flex-middle uk-flex-center',
        Cell: props => {
          return (
            <span className="uk-flex uk-flex-center">{TRANSACTION_STATUS[props.value]}</span>
          )
        }
      },
      {
        Header: () => {
          return (
            <div className="uk-flex uk-flex-middle uk-flex-center">
              <span>Price</span>
              {this.renderSort()}
            </div>
          )
        },
        accessor: 'money',
        headerClassName: 'uk-text-bold uk-text-secondary uk-background-light',
        className: 'uk-flex uk-flex-middle uk-flex-center',
        Cell: props => {
          if (props.value > 0)
            return <span><NumberFormat value={Math.abs(props.value)} displayType="text" thousandSeparator={true} prefix={'$'} /></span>
          else if (props.value == 0)
            return <span>No pay</span>
          else {
            return (
              <div>
                <b>You get: </b>
                <NumberFormat value={Math.abs(props.value)} displayType="text" thousandSeparator={true} prefix={'$'} />
              </div>
            )
          }
        }
      }
    ];

    return (
      <div className={"uk-position-relative " + (screenWidth === IS_MOBILE ? 'uk-padding-small' : '')}>
        {loadingListTransaction && <CommonLoading />}
        <ReactTable
          className="table-order"
          data={transactionsGroupByOrder}
          columns={columnsGroupByOrder}
          defaultPageSize={5}
          expanded={this.state.expandedRows}
          onExpandedChange={(newExpanded, index) => {
            if (NODE_ENV === 'development') console.log('newExpanded', newExpanded);
            this.setState((oldState) => {
              const itemIndex = index[0];
              const isExpanded = oldState.expandedRows[itemIndex];
              const expandedList = [...this.state.expandedRows];
              expandedList[itemIndex] = !isExpanded;
              return {
                expandedRows: expandedList
              };
            });
          }}
          SubComponent={
            row => {
              const { original: { transactions: transactionsOriginal } } = row;
              if (transactionsOriginal) {
                let defaultPageSize = transactionsOriginal.length;
                return (
                  <ReactTable
                    className="uk-margin-bottom"
                    data={transactionsOriginal}
                    columns={columns}
                    defaultPageSize={defaultPageSize}
                    showPagination={false} />
                )
              }
            }
          } />
      </div>
    )
  }
}
const mapStateToProps = createStructuredSelector({
  screenWidth: selectScreenWidth,
  transactionsGroupByOrder: selectTransactionsGroupByOrder,
  loadingListTransaction: selectLoadingListTransaction
})
const mapDispatchToProps = dispatch => ({
  listTransactionStart: (limit, offset) => dispatch(listTransactionStart({ limit, offset })),
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TransactionList));
