import React, { Component } from "react";

import * as Yup from "yup";
import { Formik, Form, Field } from "formik";

import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectLoadingAddDevice, selectLoadingSelectedDevice, selectSelectedDevice } from "../../redux/device/device.selectors";
import { selectScreenWidth } from "../../redux/screen/screen.selectors";
import { editDeviceStart, getDeviceStart } from "../../redux/device/device.action";

import { withRouter } from "react-router";

import Loading from "../common/CommonLoading.component";

import Slider from "rc-slider";
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const SliderWithTooltip = createSliderWithTooltip(Slider);
import NumberFormat from "react-number-format";

import CommonPageHolder from "../common/CommonPageHolder.component";
import { IS_MOBILE } from "../../config";
import CommonButton from "../common/CommonButton.component";

class DeviceAccountEdit extends Component {
  constructor(props) {
    super(props);

    this.modelSchema = Yup.object().shape({
      originalPrice: Yup.number()
        .required('Original Price must be required')
        .positive('Original Price must be positive')
        .min(1, 'Original Price must be not zero'),
      condition: Yup.number()
        .required('Condition must be required')
        .positive('Condition must be positive')
        .min(1, 'Condition must be not zero')
    });

    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    const { getDeviceStart, match: { params: { id } } } = this.props;
    getDeviceStart(id);
  }
  onSubmit(values) {
    const { history, match: { params: { id } } } = this.props;
    const { originalPrice, condition } = values;
    const device = { id, originalPrice, condition };
    this.props.editDeviceStart(device, history);
  }
  render() {
    const { loading, device, screenWidth } = this.props;

    if (device) {
      const { categoryName, imeiImei, brandName, colorName, deviceCondition, deviceOriginalPrice,
        modelName, ramName, capacityName } = device;
      return (
        <Formik
          initialValues={{
            originalPrice: deviceOriginalPrice,
            condition: deviceCondition
          }}
          validationSchema={this.modelSchema}
          onSubmit={this.onSubmit}
        >
          {
            ({ errors, touched, values, setFieldValue }) => (
              <Form>
                <div className="uk-inline uk-dark uk-width-1-1">
                  {
                    loading
                      ?
                      <Loading />
                      : null
                  }
                  <div className={screenWidth === IS_MOBILE ? '' : 'uk-child-width-1-2 uk-grid'}>
                    <div className="wc-form-group">
                      <label>
                        <span>IMEI | Serial Number</span>
                        <em>*</em>
                      </label>
                      <Field type="text" name="imei" defaultValue={imeiImei} readOnly />
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>Category</span>
                        <em>*</em>
                      </label>
                      <Field type="text" name="category" defaultValue={categoryName} readOnly />
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>Brand</span>
                        <em>*</em>
                      </label>
                      <Field type="text" name="brand" defaultValue={brandName} readOnly />
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>Model</span>
                        <em>*</em>
                      </label>
                      <Field type="text" name="model" defaultValue={modelName} readOnly />
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>Color</span>
                        <em>*</em>
                      </label>
                      <Field type="text" name="color" defaultValue={colorName} readOnly />
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>Capacity</span>
                        <em>*</em>
                      </label>
                      <Field type="text" name="capacity" defaultValue={capacityName} readOnly />
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>RAM</span>
                        <em>*</em>
                      </label>
                      <Field type="text" name="ram" defaultValue={ramName} readOnly />
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>Original Price</span>
                        <em>*</em>
                      </label>
                      {/*<Field type="number" name="originalPrice" />*/}
                      <NumberFormat name="originalPrice" thousandSeparator={true} prefix={'$'}
                        value={values['originalPrice']}
                        onValueChange={(values) => {
                          const { value } = values;
                          setFieldValue('originalPrice', value);
                        }} />
                      {errors.originalPrice && touched.originalPrice && <div className="wc-field-error">{errors.originalPrice}</div>}
                    </div>
                    <div className="wc-form-group">
                      <label>
                        <span>Condition</span>
                        <em>*</em>
                      </label>
                      <SliderWithTooltip step={10}
                        value={values['condition']}
                        onChange={(value) => setFieldValue('condition', value)} />
                      {errors.condition && touched.condition && <div className="wc-field-error">{errors.condition}</div>}
                    </div>
                    {
                      screenWidth !== IS_MOBILE
                      &&
                      <div className="wc-form-action-btn uk-flex uk-margin-large-top" />
                    }
                    <div className="wc-form-action-btn uk-flex uk-margin-large-top">
                      <div>
                        <CommonButton type="submit" className="wc-btn" screenWidth={screenWidth}>
                          Edit Device
                        </CommonButton>
                      </div>
                    </div>
                  </div>
                </div>
              </Form>
            )
          }
        </Formik>
      )
    } else
      return <CommonPageHolder />
  }
}

const mapStateToProps = createStructuredSelector({
  loadingDevice: selectLoadingSelectedDevice,
  loading: selectLoadingAddDevice,
  device: selectSelectedDevice,
  screenWidth: selectScreenWidth
});

const mapDispatchToProps = dispatch => ({
  editDeviceStart: (device, history) => dispatch(editDeviceStart({ device, history })),
  getDeviceStart: (id) => dispatch(getDeviceStart({ id }))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(DeviceAccountEdit));
