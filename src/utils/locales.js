import en_messages from "./lang/en";
import fr_messages from "./lang/fr";

const locales = [
  {
    language: 'en',
    messages: en_messages
  },
  {
    language: 'fr',
    messages: fr_messages
  }
];

export default locales;
