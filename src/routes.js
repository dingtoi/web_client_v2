import React from "react";

import CommonDynamicComponent from "./components/common/CommonDynamic.component";
import CommonEmptyPage from "./pages/common/CommonEmpty.page";
import CommonPageHolder from "./components/common/CommonPageHolder.component";

//import loadData from "./utils/loadData";

const CommonHomePage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonHome.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const CommonNotFoundPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonNotFound.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const CommonPermissionPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonPermission.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const CommonSupportPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonSupport.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const CommonContactUs = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonContactUs.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const CommonJobOppoPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonJobOppo.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const CommonTermPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonTerm.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const CommonPrivacyPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/common/CommonPrivacy.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const UserSignInAndSignUpPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/user/UserSignInAndSignUp.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const UserSignUpPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/user/UserSignUp.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const UserConfirmEmailPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/user/UserConfirmEmail.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const UserDevicePage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/user/UserDeviceList.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const UserDeviceWishlistPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/user/UserDeviceWishlist.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const UserReceivedProposalListPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/user/UserReceivedProposalList.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceMakeAvailable = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceMakeAvailable.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)

const DeviceUpdateAvailable = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceUpdateAvailable.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceAddPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceAdd.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceEditPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceEdit.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceImages = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceImages.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceCheckImeiPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceCheckImei.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceDetailAccountPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceDetailAccount.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceDetailPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceDetail.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const DeviceCategoryPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/device/DeviceCategory.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const OrderCartListPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderCartList.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const OrderProposalItemPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderProposalDetail.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const OrderProposalItemSalePage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderProposalDetailSale.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const OrderShippingPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderShipping.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)

const OrderBillingPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderBilling.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)

const OrderPaymentPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderPayment.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)

const OrderConfirmPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderConfirm.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)

const OrderListPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/order/OrderList.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)

const TransactionListPage = (props) => (
  <CommonDynamicComponent load={() => import('./pages/transaction/TransactionList.page')}>
    {
      (Component) => Component === null
        ? <CommonPageHolder />
        : <Component {...props} />
    }
  </CommonDynamicComponent>
)
const Routes = [
  {
    path: '/',
    exact: true,
    component: CommonHomePage,
    //loadData: () => loadData('posts')
  },
  {
    path: '/support',
    exact: true,
    component: CommonSupportPage
  },
  {
    path: '/contact-us',
    exact: true,
    component: CommonContactUs
  },
  {
    path: '/signInAndSignUp',
    exact: true,
    component: UserSignInAndSignUpPage
  },
  {
    path: '/signUp',
    exact: true,
    component: UserSignUpPage
  },
  {
    path: '/confirmEmail/:activeCode',
    exact: true,
    component: UserConfirmEmailPage
  },
  {
    path: '/device/:id',
    exact: true,
    component: DeviceDetailPage
  },
  {
    path: '/bags',
    exact: true,
    component: OrderCartListPage
  },
  {
    path: '/account',
    exact: true,
    component: UserDevicePage
  },
  {
    path: '/account/make-available/:id',
    exact: true,
    component: DeviceMakeAvailable
  },
  {
    path: '/account/update-available/:id',
    exact: true,
    component: DeviceUpdateAvailable
  },
  {
    path: '/account/device/images/:id',
    exact: true,
    component: DeviceImages
  },
  {
    path: '/account/device',
    exact: true,
    component: UserDevicePage
  },
  {
    path: '/account/received-proposal',
    exact: true,
    component: UserReceivedProposalListPage
  },
  {
    path: '/account/received-proposal/exchange/:id',
    exact: true,
    component: OrderProposalItemPage
  },
  {
    path: '/account/received-proposal/sale/:id',
    exact: true,
    component: OrderProposalItemSalePage
  },
  {
    path: '/account/device/:id',
    exact: true,
    component: DeviceDetailAccountPage
  },
  {
    path: '/account/wishlist',
    exact: true,
    component: UserDeviceWishlistPage
  },
  {
    path: '/account/add-device',
    exact: true,
    component: DeviceAddPage
  },
  {
    path: '/account/edit-device/:id',
    exact: true,
    component: DeviceEditPage
  },
  {
    path: '/account/check-imei',
    exact: true,
    component: DeviceCheckImeiPage
  },
  {
    path: '/account/order',
    exact: true,
    component: OrderListPage
  },
  {
    path: '/account/transaction',
    exact: true,
    component: TransactionListPage
  },
  {
    path: '/category',
    exact: true,
    component: DeviceCategoryPage
  },
  {
    path: '/empty',
    exact: true,
    component: CommonEmptyPage
  },
  {
    path: '/permission',
    exact: true,
    component: CommonPermissionPage
  },
  {
    path: '/job-opportunities',
    exact: true,
    component: CommonJobOppoPage
  },
  {
    path: '/term-of-use',
    exact: true,
    component: CommonTermPage
  },
  {
    path: '/order/shipping',
    exact: true,
    component: OrderShippingPage
  },
  {
    path: '/order/billing',
    exact: true,
    component: OrderBillingPage
  },
  {
    path: '/order/payment',
    exact: true,
    component: OrderPaymentPage
  },
  {
    path: '/order/confirm',
    exact: true,
    component: OrderConfirmPage
  },
  {
    path: '/privacy',
    exact: true,
    component: CommonPrivacyPage
  },
  {
    component: CommonNotFoundPage
  }

];

export default Routes;
