import React, { Component } from "react";
import CommonBannerCategory from "../../components/common/CommonBannerCategory.component";
import CommonBannerTopSale from "../../components/common/CommonBannerTopSale.component";
import withApp from "../../hoc/withApp.hoc";
import FeaturedDevices from "../../components/device/DeviceFeaturedList.component";
import CommonSlideshow from "../../components/common/CommonSlideshow.component";
import { IS_MOBILE } from "../../config";
import DeviceSellExchangeList from "../../components/device/DeviceSellExchangeList.component";
import CommonBrandMenu from "../../components/common/CommonBrandMenu.component";

import { connect } from "react-redux";

import { setCategoryId } from "../../redux/screen/screen.actions";

import { withRouter } from "react-router";
import { historyRedirect } from "../../utils";

class HomePage extends Component {
  constructor(props) {
    super(props);
  }
  handleBrandClick(brand) {
    this.props.setCategoryId('brandIds', [brand.id]);
    const { history } = this.props;
    setTimeout(() => {
      historyRedirect({ history, uri: 'category' });
    }, 500);
  }
  render() {
    const { screenWidth } = this.props;
    return (
      <div>
        <CommonSlideshow />
        {screenWidth === IS_MOBILE && <DeviceSellExchangeList />}
        {screenWidth !== IS_MOBILE && <CommonBannerCategory />}
        {screenWidth === IS_MOBILE && <CommonBrandMenu onClick={(brand) => this.handleBrandClick(brand)} />}
        <FeaturedDevices />
        {screenWidth === IS_MOBILE && <CommonBannerCategory />}
        <CommonBannerTopSale />
        {screenWidth !== IS_MOBILE && <DeviceSellExchangeList />}
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  setCategoryId: (field, value) => dispatch(setCategoryId(field, value))
});

export default withApp(connect(null, mapDispatchToProps)(withRouter(HomePage)));
