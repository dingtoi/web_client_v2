import React, { Component, Fragment } from "react";
import withApp from "../../hoc/withApp.hoc";
import { IS_MOBILE } from "../../config";
import DeviceUpload from "../../components/device/DeviceUpload.component";

import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectSelectedDevice } from "../../redux/device/device.selectors";

import { withRouter } from "react-router";
import { selectScreenWidth } from "../../redux/screen/screen.selectors";
import CommonBreadcrumb from "../../components/common/CommonBreadcrumb.component";

class DeviceImages extends Component {
  constructor(props) {
    super(props);
  }
  renderBreadcrumb() {
    const { device, screenWidth } = this.props;
    if (screenWidth !== IS_MOBILE) {
      if (device) {
        const { deviceId } = device;
        return (
          <CommonBreadcrumb list={[{ name: 'List Devices', uri: 'account/device' }, { name: 'Device Detail', uri: 'account/device/' + deviceId }, { name: 'Device Images' }]} />
        )
      }
    }
  }
  renderUploadName() {
    const { device } = this.props;
    if (device) {
      const { modelDetailName } = device;
      return (
        <div className="uk-text-meta uk-margin-top uk-margin-small-bottom">Upload Images for <b>{modelDetailName}</b>.</div>
      )
    }
  }
  renderMobile() {
    const { device, screenWidth } = this.props;

    return (
      <Fragment>
        <div className="uk-position-relative">
          {this.renderBreadcrumb()}
          {screenWidth === IS_MOBILE
            ?
            <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle uk-flex-between uk-container">
              <div className="uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">
                Device Images
              </div>
            </div>
            :
            <div className="uk-container uk-margin-top">
              <article className="uk-article">
                <h3>Device Images</h3>
                <hr />
              </article>
              {this.renderUploadName()}
            </div>
          }
          <div className="uk-container uk-margin-top">
            <DeviceUpload device={device} />
          </div>
        </div>
      </Fragment>
    )
  }
  render() {
    const { screenWidth } = this.props;

    if (screenWidth === IS_MOBILE)
      return this.renderMobile();
    else
      return this.renderMobile();
  }
}

const mapStateToProps = createStructuredSelector({
  screenWidth: selectScreenWidth,
  device: selectSelectedDevice
});

export default withApp(connect(mapStateToProps)(withRouter(DeviceImages)));
