import React, { Component, Fragment } from "react";
import withApp from "../../hoc/withApp.hoc";
import { withRouter } from "react-router-dom";

import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectScreenWidth } from "../../redux/screen/screen.selectors";
import { IS_MOBILE } from "../../config";
import DeviceEdit from "../../components/device/DeviceAccountEdit.component";
import CommonBreadcrumb from "../../components/common/CommonBreadcrumb.component";

class DeviceEditPage extends Component {
  constructor(props) {
    super(props);
  }
  renderMobile() {
    const { screenWidth, match: { params: { id } } } = this.props;
    return (
      <Fragment>
        {screenWidth !== IS_MOBILE && <CommonBreadcrumb list={[{ name: 'Device Detail', uri: 'account/device/' + id }, { name: 'Edit Device' }]} />}
        {
          screenWidth === IS_MOBILE
            ?
            <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle">
              <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">
                Edit a Device
              </div>
            </div>
            :
            <div className="uk-container uk-margin-top">
              <article className="uk-article">
                <h3>Edit A Device</h3>
                <hr />
              </article>
              <div className="uk-text-meta uk-margin-top uk-margin-small-bottom">Edit Device for your system.</div>
            </div>
        }
        <div className="uk-container uk-margin-top">
          <div className="wc-block-wrapper">
            <DeviceEdit />
          </div>
        </div>
      </Fragment>
    )
  }
  render() {
    const { screenWidth } = this.props;

    if (screenWidth === IS_MOBILE)
      return this.renderMobile();
    else
      return this.renderMobile();
  }
}

const mapStateToProps = createStructuredSelector({
  screenWidth: selectScreenWidth
});

export default withApp(connect(mapStateToProps)(withRouter(DeviceEditPage)));
