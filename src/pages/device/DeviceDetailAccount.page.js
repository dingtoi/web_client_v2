import React, { Component, Fragment } from "react";

import { withRouter } from "react-router";

import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectLoadingSelectedDevice, selectSelectedDevice, selectLoadingDelete } from "../../redux/device/device.selectors";
import { getDeviceStart, deleteDeviceStart, removeAvailableStart } from "../../redux/device/device.action";

import { IS_MOBILE } from "../../config";
import withApp from "../../hoc/withApp.hoc";
import { historyRedirect } from "../../utils";

import check from "check-types";

import Popup from "reactjs-popup";
import CommonModalConfirm from "../../components/common/CommonModalConfirm.component";
import DeviceImages from "../../components/device/DeviceImages.component";
import CommonBreadcrumb from "../../components/common/CommonBreadcrumb.component";
import CommonLoading from "../../components/common/CommonLoading.component";
import NumberFormat from "react-number-format";
import CommonPageHolder from "../../components/common/CommonPageHolder.component";
import CommonButton from "../../components/common/CommonButton.component";


class DeviceDetailAccountPage extends Component {
  constructor(props) {
    super(props);
    this.onRedirect = this.onRedirect.bind(this);
  }
  componentDidMount() {
    const { getDeviceStart, match: { params: { id } } } = this.props;
    getDeviceStart(id);
  }
  onRedirect(url) {
    const { history } = this.props;
    historyRedirect({ history, uri: url });
  }
  renderActions(closeParent) {
    const { history, deleteDeviceStart,
      device: { isAvailableDevice, availableDevice },
      match: { params: { id } }, screenWidth, removeAvailableStart } = this.props;

    return (
      <ul className="uk-nav-sub">
        <li>
          <a
            onClick={this.onRedirect.bind(this, 'account/device/images/' + id)}
          >Choose Images</a>
        </li>
        {
          !isAvailableDevice
          &&
          <li>
            <a
              onClick={this.onRedirect.bind(this, 'account/edit-device/' + id)}>
              Edit Device
          </a>
          </li>
        }
        <li>
          {isAvailableDevice
            &&
            <CommonModalConfirm
              screenWidth={screenWidth}
              onClose={() => closeParent()}
              message="Do you really want to remove available ?"
              onOk={close => {
                removeAvailableStart(availableDevice.availableDeviceId, id, history);
                close();
              }}
              trigger={<a>Remove Available</a>}
            />
          }
        </li>
        <li>
          <CommonModalConfirm
            screenWidth={screenWidth}
            onClose={() => closeParent()}
            message="Do you really want to delete that device ?"
            onOk={close => {
              deleteDeviceStart(id, history);
              close();
            }}
            trigger={<a>Delete Device</a>}
          />
        </li>
      </ul>
    )
  }
  renderAvaiNone() {
    const { screenWidth } = this.props;

    let classWidth = ['uk-flex uk-flex-between'];

    if(screenWidth !== IS_MOBILE)
      classWidth.push('uk-width-medium');

    return (
      <li>
        <div className={classWidth.join(' ')}>
          <div className="uk-text-bold">Available Type</div>
          <div>None</div>
        </div>
      </li>
    )
  }
  renderSale() {
    const { device, screenWidth } = this.props;
    const { availableDevice } = device;
    const { availableDeviceSalePrice } = availableDevice;

    let classWidth = ['uk-flex uk-flex-between'];

    if(screenWidth !== IS_MOBILE)
      classWidth.push('uk-width-medium');

    return (
      <Fragment>
        <li>
          <div className={classWidth.join(' ')}>
            <div className="uk-text-bold">Available Type</div>
            <div>Sale</div>
          </div>
        </li>
        <li>
          <div className={classWidth.join(' ')}>
            <div className="uk-text-bold">Sale Price</div>
            <div>
              <div className="uk-text-right">
                <NumberFormat value={availableDeviceSalePrice} displayType="text" thousandSeparator={true} prefix={'$'} />
              </div>
            </div>
          </div>
        </li>
      </Fragment>
    )
  }
  renderPriceExchange() {
    const { device } = this.props;
    const { availableDevice } = device;
    const { availableDeviceExchangePrice, exchangeModelName } = availableDevice;

    if(!check.negative(availableDeviceExchangePrice)) {
      if (availableDeviceExchangePrice === 0) {
        return (
          <div className="uk-text-right">
            Exchange with <b>{exchangeModelName}</b>
          </div>
        )
      } else {
        return (
          <div className="uk-text-right">
            Pay&nbsp;
            <NumberFormat value={availableDeviceExchangePrice} displayType="text" thousandSeparator={true} prefix={'$'} />
            &nbsp;for <b>{exchangeModelName}</b>
          </div>
        )
      }
    } else {
      return (
        <div className="uk-text-right">
          Receive&nbsp;
          <NumberFormat value={Math.abs(availableDeviceExchangePrice)} displayType="text" thousandSeparator={true} prefix={'$'} />
          &nbsp;from <b>{exchangeModelName}</b>
        </div>
      )
    }
  }

  renderExchange() {
    const { screenWidth } = this.props;

    let classWidth = ['uk-flex uk-flex-between'];

    if(screenWidth !== IS_MOBILE)
      classWidth.push('uk-width-medium');

    return (
      <Fragment>
        <li>
          <div className={classWidth.join(' ')}>
            <div className="uk-text-bold">Available Type</div>
            <div>Exchange</div>
          </div>
        </li>
        <li>
          <div className={classWidth.join(' ')}>
            <div className="uk-text-bold">Exchange Price</div>
            <div>
              {this.renderPriceExchange()}
            </div>
          </div>
        </li>
      </Fragment>
    )
  }
  renderAvailable() {
    const { device, screenWidth } = this.props;
    const { isAvailableDevice, modelDetailName, availableDevice } = device;
    let blockAvailable = null;

    let classUL = ['uk-list uk-text-small'];
    if(screenWidth === IS_MOBILE)
      classUL.push('uk-list-striped');

    if (isAvailableDevice) {
      if(availableDevice){
        const { availableDeviceType } = availableDevice;
        switch (availableDeviceType) {
          case 'exchange':
            blockAvailable = (
              this.renderExchange()
            )
            break;
          case 'sell':
            blockAvailable = (
              this.renderSale()
            )
            break;
          default:
            break;
        }
      }
    }else
      blockAvailable = (
        this.renderAvaiNone()
      )
    return (
      <Fragment>
        <div className="uk-text-bold uk-text-lead">{modelDetailName}</div>

        <ul className={classUL.join(' ')}>
          {blockAvailable}
        </ul>
      </Fragment>
    )
  }
  renderSpecs() {
    const { device, screenWidth } = this.props;

    if (device) {
      const { categoryName, imeiImei, brandName, colorName, deviceCondition, deviceOriginalPrice,
        modelName, ramName, capacityName } = device;
      let arrSpec = [
        {t: 'Imei', v: imeiImei},
        {t: 'Category', v: categoryName},
        {t: 'Brand', v: brandName},
        {t: 'Model', v: modelName},
        {t: 'Color', v: colorName},
        {t: 'Capacity', v: capacityName},
        {t: 'RAM', v: ramName},
        {t: 'Original Price', v: <NumberFormat value={deviceOriginalPrice} displayType="text" thousandSeparator={true} prefix={'$'} />},
        {t: 'Device Condition', v: deviceCondition+' %'}
      ];

      if(screenWidth === IS_MOBILE)
        return (
          <Fragment>
            <div className="uk-text-bold uk-text-lead">System Configuration</div>
            <ul className="uk-list uk-text-small uk-list-striped">
            {
              arrSpec.map((spec, index) => {
                return (
                  <li key={index}>
                    <div className="uk-flex uk-flex-between uk-width-1-1">
                      <div className="uk-text-bold">{spec.t}</div>
                      <div className="uk-text-right">{spec.v}</div>
                    </div>
                  </li>
                )
              })
            }
            </ul>
          </Fragment>
        )
      else
        return (
          <Fragment>
            <div className="uk-text-bold uk-text-lead">System Configuration</div>
            <ul className="uk-list uk-text-small">
            {
              arrSpec.map((spec, index) => {
                return (
                  <li key={index}>
                    <div className="uk-flex uk-flex-between uk-width-medium">
                      <div className="uk-text-bold">{spec.t}</div>
                      <div className="uk-text-right">{spec.v}</div>
                    </div>
                  </li>
                )
              })
            }
            </ul>
          </Fragment>
        )
    }
  }
  renderCommands() {
    const { screenWidth, history, match: { params: { id } }, device } = this.props;
    const { isAvailableDevice, availableDevice } = device;

    if(screenWidth === IS_MOBILE)
      return (
        <div className="wc-navbar-wrapper-footer">
          <div className="uk-flex">
            <div className="wc-navbar-link-wrapper uk-width-1-1">
              <div className="uk-flex">
                <Popup
                  trigger={
                    <div className="uk-width-1-2">
                      <CommonButton screenWidth={screenWidth}
                        className="wc-btn navbar-button uk-margin-remove">
                        Actions
                      </CommonButton>
                    </div>
                  }
                  on="click"
                  closeOnDocumentClick
                  mouseLeaveDelay={300}
                  mouseEnterDelay={0}
                  position="top left"
                  contentStyle={{ border: "none", padding: 0, zIndex: 9999 }}
                  arrow={false}>
                  {close => this.renderActions(close)}
                </Popup>
                <div className="uk-width-1-2">
                  {
                    !isAvailableDevice
                    ?
                    <CommonButton screenWidth={screenWidth}
                      onClick={() => historyRedirect({ history, uri: 'account/make-available/'+id })}
                      className="wc-btn navbar-button uk-text-white uk-margin-remove" type="inverted">
                      Post Device
                    </CommonButton>
                    :
                    <CommonButton screenWidth={screenWidth}
                      onClick={() => historyRedirect({ history, uri: 'account/update-available/'+availableDevice.availableDeviceId })}
                      className="wc-btn navbar-button uk-text-white uk-margin-remove" type="inverted">
                      Update Available
                    </CommonButton>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    else{
      return (
        <div className="uk-flex">
          <Popup
            trigger={
              <div>
                <CommonButton screenWidth={screenWidth}
                  className="wc-btn uk-margin-xsmall-right">
                  Actions
                </CommonButton>
              </div>
            }
            on="click"
            closeOnDocumentClick
            mouseLeaveDelay={300}
            mouseEnterDelay={0}
            position="top left"
            contentStyle={{ border: "none", padding: 0, zIndex: 9999 }}
            arrow={false}>
            {close => this.renderActions(close)}
          </Popup>
          {
            isAvailableDevice
            ?
            <CommonButton screenWidth={screenWidth}
              onClick={() => historyRedirect({ history, uri: 'account/update-available/'+availableDevice.availableDeviceId })}
              className="wc-btn" type="inverted">
              Update Available
            </CommonButton>
            :
            <CommonButton screenWidth={screenWidth}
              onClick={() => historyRedirect({ history, uri: 'account/make-available/'+id })}
              className="wc-btn" type="inverted">
              Post Device
            </CommonButton>
          }
        </div>
      )
    }
  }
  renderPC() {
    const { loadingDelete, device, match: { params: { id } }, screenWidth,
      loading } = this.props;

    if (device) {
      return (
        <Fragment>
          <CommonBreadcrumb list={[{ name: 'List Devices', uri: 'account/device' }, { name: 'Device' }]} />
          <div className="uk-container uk-margin-medium-top uk-margin-medium-bottom uk-position-relative">
            {
              (loadingDelete || loading) && <CommonLoading />
            }
            <div className="uk-grid">
              <div className="uk-width-1-2 uk-position-relative">
                <div className="uk-position-top-right uk-overlay-default
                  uk-background-active wc-available wc-badge-image"
                  onClick={() => {this.onRedirect('account/device/images/' + id)}}>Select Images</div>
                <DeviceImages device={device} screenWidth={screenWidth} />
              </div>
              <div className="uk-width-1-2">
                {this.renderAvailable()}
                {this.renderCommands()}
                {this.renderSpecs()}
              </div>
            </div>
          </div>
        </Fragment>
      )
    } else return <CommonPageHolder />;
  }
  renderMobile() {
    const { loadingDelete, device, screenWidth, match: {params: {id}},
      loading } = this.props;

    if (device) {
      return (
        <Fragment>
          <div className="uk-position-relative">
            <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle uk-flex-between uk-container">
              <div className="uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">
                Device Detail
              </div>
            </div>
            <div className="uk-container uk-margin-top">
              {
                (loadingDelete || loading) && <CommonLoading />
              }
              <div className="wc-block-wrapper">
                <div className="uk-padding uk-position-relative">
                <div className="uk-position-top-right uk-overlay-default
                  uk-background-active wc-available wc-badge-image"
                  onClick={() => {this.onRedirect('account/device/images/' + id)}}>Select Images</div>
                  <DeviceImages device={device} screenWidth={screenWidth} />
                </div>
                {this.renderAvailable()}
                <hr />
                {this.renderSpecs()}
              </div>
            </div>
          </div>
          {this.renderCommands()}
        </Fragment>
      )
    } else return <CommonPageHolder />;

  }
  render() {
    const { screenWidth } = this.props;

    if (screenWidth === IS_MOBILE)
      return this.renderMobile();
    else
      return this.renderPC();
  }
}

const mapStateToProps = createStructuredSelector({
  loading: selectLoadingSelectedDevice,
  loadingDelete: selectLoadingDelete,
  device: selectSelectedDevice
});

const mapDispatchToProps = dispatch => ({
  getDeviceStart: (id) => dispatch(getDeviceStart({ id })),
  deleteDeviceStart: (id, history) => dispatch(deleteDeviceStart({ id, history })),
  removeAvailableStart: (id, deviceId, history) => dispatch(removeAvailableStart({ id, deviceId, history }))
});

export default withApp(connect(mapStateToProps, mapDispatchToProps)(withRouter(DeviceDetailAccountPage)));
